﻿using System;
using System.Collections.Generic;

// Фабричный метод - застройщики и дома
// Мост - строители, их тип и качество работы
// Шаблонный метод - этапы строительста домов
namespace Patterny
{
  // абстрактный класс строительной компании
  abstract class Developer
  {
    public string Name { get; set; }
    public Stroitel Stroitel;
    public Developer(string n, Stroitel s)
    {
      Name = n;
      Stroitel = s;
    }
    // фабричный метод
    abstract public House Create();
  }

  class PanelDeveloper : Developer
  {
    public PanelDeveloper(string n, Stroitel s) : base(n,s)
    { }

    public override House Create()
    {
      return new PanelHouse();
    }
  }

  class WoodDeveloper : Developer
  {
    public WoodDeveloper(string n, Stroitel s) : base(n,s)
    { }

    public override House Create()
    {
      return new WoodHouse();
    }
  }



  public abstract class House
  {
    public House(string n)
    {
      this.Name = n;
    }
    public string Name { get; protected set; }
    public abstract int GetCost();

    // Шаблонный метод
    public void Building()
    {
      BuyMaterials();
      CreateFundament();
      CreateSteny();
      CreateRoof();
    }
    
    public abstract void BuyMaterials();
    public virtual void CreateFundament()
    {
      Console.WriteLine("Делаем фундамент из бетона");
    }
    public abstract void CreateSteny();
    public abstract void CreateRoof();
  }

  class PanelHouse : House
  {
    public PanelHouse() : base("Панельный дом")
    { }
    public override int GetCost()
    {
      return 1000000;
    }
    public override void BuyMaterials()
    {
      Console.WriteLine("Покупаем материалы для панельного дома");
    }

    public override void CreateSteny()
    {
      Console.WriteLine("Делаем стены из панелей");
    }

    public override void CreateRoof()
    {
      Console.WriteLine("Делаем крышу из композитной кровли");
    }
  }
  class WoodHouse : House
  {
    public WoodHouse()
        : base("Деревянный дом")
    { }
    public override int GetCost()
    {
      return 2000000;
    }
    public override void BuyMaterials()
    {
      Console.WriteLine("Покупаем материалы для деревянного дома");
    }

    public override void CreateSteny()
    {
      Console.WriteLine("Делаем стены из дерева");
    }

    public override void CreateRoof()
    {
      Console.WriteLine("Делаем крышу из дерева");
    }
  }

  //мост
  interface TypeOfBuilding
  {
    void Build();
  }

  class ProfBuilding : TypeOfBuilding
  {
    public void Build()
    {
      Console.WriteLine("Качественное и быстрое выполнение работ без каких-либо консультаций, так как специалист работает по профилю");
    } 
  }

  class NovichokBuilding : TypeOfBuilding
  {
    public void Build()
    {
      Console.WriteLine("Выполнение работ с консльтациями прораба и с невысоким качеством и скоростью, так как специалист работает не по профилю");
    }

  }

  abstract class Stroitel
  {
    protected TypeOfBuilding typeOfBuilding;
    public TypeOfBuilding TypeOfBuilding
    {
      set { typeOfBuilding = value; }
    }
    public Stroitel(TypeOfBuilding type)
    {
      typeOfBuilding = type;
    }
    public virtual void DoWork()
    {
      typeOfBuilding.Build();
    }
    public abstract void TypeOfDohod();
  }

  class FreelanceStroitel : Stroitel
  {
    public FreelanceStroitel(TypeOfBuilding type) : base(type)
    {
    }
    public override void TypeOfDohod()
    {
      Console.WriteLine("Строитель по найму получает деньги за выполненную работу");
    }
  }
  class StroitelFirmy : Stroitel
  {
    public StroitelFirmy(TypeOfBuilding type)
        : base(type)
    {
    }
    public override void TypeOfDohod()
    {
      Console.WriteLine("Строитель, работающий постоянно в фирме получает деньги в конце месяца");
    }
  }





  class Program
  {
    static void Main(string[] args)
    {
      Stroitel s1 = new FreelanceStroitel(new ProfBuilding());
      Developer dev = new PanelDeveloper("ООО Жить в панеле", s1);
      House h1 = dev.Create();
      Console.WriteLine("Застройщик: {0}", dev.Name);
      Console.WriteLine("Название: {0}", h1.Name);
      Console.WriteLine("Цена: {0}", h1.GetCost());
      Console.WriteLine("\nЭтапы строительства");
      h1.Building();
      Console.WriteLine("\nВыполнение первой части работ строителем-фрилансером: ");
      dev.Stroitel.TypeOfDohod();
      dev.Stroitel.DoWork();
      Console.WriteLine("\nВыполнение второй части работ строителем-фрилансером: ");
      s1.TypeOfBuilding = new NovichokBuilding();
      dev.Stroitel.DoWork();
      Console.WriteLine("\n------");

      Stroitel s2 = new StroitelFirmy(new NovichokBuilding());
      Developer dev2 = new WoodDeveloper("ООО Жить в дереве", s2);
      House h2 = dev2.Create();
     
      Console.WriteLine("Застройщик: {0}", dev2.Name);
      Console.WriteLine("Название: {0}", h2.Name);
      Console.WriteLine("Цена: {0}", h2.GetCost());
      Console.WriteLine("\nЭтапы строительства");
      h2.Building();
      Console.WriteLine("\nВыполнение первой части работ строителем на постоянном трудовом договоре: ");
      dev2.Stroitel.TypeOfDohod();
      dev2.Stroitel.DoWork();
      Console.WriteLine("\nВыполнение второй части работ строителем на постоянном трудовом договоре: ");
      s1.TypeOfBuilding = new ProfBuilding();
      dev.Stroitel.DoWork();
      Console.WriteLine("\n------");



      Console.ReadKey();
    }
  }
}
